#ifndef MODEL_H
#define MODEL_H

#include <QAbstractItemModel>
#include <QVariant>

#include "node.h"


class Model : public QAbstractItemModel
{
public:
    Model();

    QVariant data(const QModelIndex &index, int role) const;
    // Qt::ItemFlags flags(const QModelIndex &index) const;
//    QVariant headerData(int section, Qt::Orientation orientation,
//                        int role = Qt::DisplayRole) const;
    QModelIndex	index ( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

private:
    std::vector<Node> mData;
};

#endif // MODEL_H
