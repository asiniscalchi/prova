#include <QDebug>
#include "model.h"

Model::Model()
{
    for (int i=0 ; i < 100 ; ++i)
        mData.push_back(Node(i));
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    qDebug() << "(DD)Model::data|role = " << role;
    if (role == Qt::DisplayRole){
        Node n = mData.at(index.row());

        if (index.column() == 0)
            return QVariant(n.x);
        else if (index.column() == 1)
            return QVariant(n.y);
        else
            return QVariant(n.z);
}

    return QVariant();
}

/*
// Qt::ItemFlags flags(const QModelIndex &index) const;
QVariant Model::headerData(int section, Qt::Orientation orientation,
                           int role = Qt::DisplayRole) const
{
    return QVariant();
}
*/

QModelIndex Model::index(int row, int column,
                         const QModelIndex &parent) const
{
    qDebug() << "(DD)Model::index(" << row << ", " << column << ")" << (parent.isValid() ? "valid" : "invalid");


    if (parent.isValid())
        return QModelIndex();

    return createIndex(row, column);
}

QModelIndex Model::parent(const QModelIndex &index) const
{
    qDebug() << "(DD)Model::parent";
    return QModelIndex();
}

int Model::rowCount(const QModelIndex &parent) const
{
    return mData.size();
}

int Model::columnCount(const QModelIndex &parent) const
{
    return 3;
}
