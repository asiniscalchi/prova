#include <QDebug>
#include <QApplication>
#include <QSplitter>
#include <QTableView>
#include <QListView>
#include <QTreeView>

#include "model.h"
#include "delegate.h"

int main(int argc, char *argv[])
{
    qDebug() << "start";

    QApplication app(argc, argv);

    QSplitter *splitter = new QSplitter;

    //initialize the model
    QAbstractItemModel *model = new Model;

    // initialize delegate
    QAbstractItemDelegate *delegate = new Delegate;

    QTableView *table = new QTableView(splitter);
    table->setItemDelegate(delegate);
    table->setModel(model);

    QListView *list = new QListView(splitter);
    //list->setItemDelegate(delegate);
    list->setModel(model);

    QTreeView *tree = new QTreeView(splitter);
    tree->setItemDelegate(delegate);
    tree->setModel(model);

    splitter->show();

    return app.exec();
}
