#include "delegate.h"

#include <QPainter>
#include <QSize>
#include <QRect>
#include <QApplication>

Delegate::Delegate()
{
}


void Delegate::paint(QPainter *painter,
                   const QStyleOptionViewItem &option,
                   const QModelIndex &index) const
{
    if (index.column() == 2) {
             int progress = index.data().toInt();

             QStyleOptionProgressBar progressBarOption;
             progressBarOption.rect = option.rect;
             progressBarOption.minimum = 0;
             progressBarOption.maximum = 100;
             progressBarOption.progress = progress;
             progressBarOption.text = QString::number(progress) + "%";
             progressBarOption.textVisible = true;

             QApplication::style()->drawControl(QStyle::CE_ProgressBar,
                                                &progressBarOption, painter);
         }
    else if (index.column() == 1)
    {
        QStyleOptionButton button;
        button.rect = option.rect;
        button.text = index.data().toString();
        QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);
    }else {
    QRect rect(option.rect.adjusted(4,4,-4,-4));
    rect.setWidth(rect.width()*index.data().toInt()/100);

    painter->fillRect(rect, QColor("steelblue").lighter(index.data().toInt()));
    painter->drawText(option.rect, index.data().toString());
    }
}

QSize Delegate::sizeHint(const QStyleOptionViewItem &option,
                       const QModelIndex &index) const
{
    //Q_UNUSED(index)
    // TODO: what's the first param of QSize
    return QSize(1, option.fontMetrics.height());
}
