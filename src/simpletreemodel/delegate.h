#ifndef DELEGATE_H
#define DELEGATE_H

#include <QAbstractItemDelegate>

class Delegate : public QAbstractItemDelegate
{
public:
    Delegate();

    void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};

#endif // DELEGATE_H
